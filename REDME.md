# Laravel Tracker - Kelvyn Carbone

[![Latest Stable Version](https://img.shields.io/packagist/v/pimlie/laravel-datatables-mongodb.svg)](https://packagist.org/packages/pimlie/laravel-datatables-mongodb)
[![Total Downloads](https://img.shields.io/packagist/dt/pimlie/laravel-datatables-mongodb.svg)](https://packagist.org/packages/pimlie/laravel-datatables-mongodb)
[![License](https://img.shields.io/github/license/pimlie/laravel-datatables-mongodb.svg)](https://packagist.org/packages/pimlie/laravel-datatables-mongodb)

This package is for tracking the user with some informations and crossing data making a map of user and that he makes.

## Requirements
- "jenssegers/mongodb": "3.2"
- "yajra/laravel-datatables-oracle":"~8.0",
- "khill/lavacharts": "3.0.*"

## Installation
```composer require kelvyncarbone/tracker:dev-master```

## Configure
Set the providers at config/app.php

```
'providers' => [
    ...,
        KelvynCarbone\Tracker\TrackerServiceProvider::class,
        Jenssegers\Mongodb\MongodbServiceProvider::class,
        Yajra\DataTables\DataTablesServiceProvider::class,
]
```
Set the aliases at config/app.php
```
'aliases' => [
    ...,
        'DataTables' => Yajra\DataTables\Facades\DataTables::class
]
```
Set the mongodb connection at config/database.php
```
'tracker' => [
            'driver' => 'mongodb',
            'host' => '{your_mongodb_host}',
            'port'  => 27017,
            'database' => '{your_mongodb_database}',
            'username' => '{your_mongodb_admin}',
            'password' => '{your_mongodb_password}'
        ]
```

## Usage
For this to work you need to make a publish

```
php artisan vendor:publish

Now you have new files in your structure

See the TrackerController
```
