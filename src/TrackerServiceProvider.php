<?php
namespace KelvynCarbone\Tracker;

use Illuminate\Support\ServiceProvider as SupportServiceProvider;
use KelvynCarbone\Tracker\Tracker;

class TrackerServiceProvider extends SupportServiceProvider
{
    public function boot(){
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/views','tracker');
        $this->publishes([
           __DIR__ . '/views' => resource_path('views/tracker'),
            __DIR__ . '/Models' => base_path('app/Entities'),
            __DIR__.'/TrackerController.php' => base_path('app/Http/Controllers/TrackerController.php'),
            __DIR__.'/config/tracker.php', config_path('tracker.php')
        ]);
    }

    public function register(){
        $this->app->singleton(Tracker::class,function($app){
            return new Tracker();
        });

        $this->app->alias(Tracker::class,'Tracker');
    }

    public function provides(){
        return ['Tracker'];
    }
}