<?php

namespace KelvynCarbone\Tracker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Khill\Lavacharts\Lavacharts;
use Yajra\Datatables\Datatables;

class TrackerController extends Controller
{
    protected $tracker;

    public function __construct(Tracker $tracker)
    {
        $this->tracker = $tracker;
    }

    public function index()
    {
        $lava = new Lavacharts; // See note below for Laravel
        $reasons = $lava->DataTable();
        $geo = $this->tracker->totalGeolocations();
        $emails = $this->tracker->totalEmails();
        $products = $this->tracker->totalProducts();
        $one_signals = $this->tracker->totalOneSignals();
        $data = $this->tracker->grid();

        $reasons->addStringColumn('Reasons')
            ->addNumberColumn('Total')
            ->addRow(['Geolocalizações', $geo])
            ->addRow(['Produtos', $products])
            ->addRow(['One Signals', $one_signals])
            ->addRow(['E-mails', $emails]);

        $lava->BarChart('IMDB', $reasons, [
            'title' => 'Total de dados capturados por categoria'
        ]);

        return view('tracker::index')
            ->with("lava", $lava)
            ->with("data", $data);
    }

    public function show()
    {
        return view('tracker::show');
    }

    public function create()
    {

    }

    public function destroy()
    {

    }

    public function store(Request $request)
    {
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization, X-CSRF-Token');
        header('Access-Control-Allow-Credentials: true');

        $inputs = $request->all();
        if (!$request->has("tracker_id")) {
            $tracker = $this->tracker->create($inputs);
            return response()->json(["tracker_id" => $tracker->getQueueableId()]);
        } else {
            $tracker = $this->tracker->update($inputs);
            return response()->json(["tracker" => $tracker]);
        }
    }
}