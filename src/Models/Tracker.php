<?php
namespace KelvynCarbone\Tracker\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Tracker extends Eloquent
{
    use SoftDeletes;
    protected $connection = "tracker";
    protected $collection = "tracker";
}