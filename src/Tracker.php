<?php

namespace KelvynCarbone\Tracker;

use Illuminate\Support\Facades\DB;
use KelvynCarbone\Tracker\Models\Tracker AS TrackerModel;
use Exception;

class Tracker
{
    public function create(array $inputs)
    {
        if (count($inputs) == 0)
            throw new Exception('Não há dados para efetuar a criação do registro.');
        if (isset($inputs["token"]))
            unset($inputs["token"]);
        if (!isset($inputs["emails"]) && !isset($inputs["one_signal"]))
            throw new Exception('Não há dados para efetuar a criação do registro.');

        $tracker = new TrackerModel();
        foreach ($inputs AS $key => $input) {
            if ($key == "geolocation" && is_array($input))
                $data["loc"] = ["x" => $input[0], "y" => $input[1]];
            else
                $tracker[$key] = explode(",", $input);
        }
        $tracker["created_at"] = new \MongoDB\BSON\UTCDateTime(new \DateTime('now'));
        $tracker->save();
        return $tracker->getQueueableId();
    }

    public function update(array $inputs)
    {
        if (count($inputs) == 0)
            throw new Exception('Não há dados para efetuar a criação do registro.');
        if (isset($inputs["token"]))
            unset($inputs["token"]);
        if (!isset($inputs["emails"]) && !isset($inputs["one_signal"]))
            throw new Exception('Não há dados para efetuar a criação do registro.');

        $tracker_id = $inputs["tracker_id"];
        unset($inputs["tracker_id"]);
        foreach ($inputs AS $key => $input) {
            if ($key == "geolocation" && is_array($input))
                $data["loc"] = ["x" => $input[0], "y" => $input[1]];
            else
                $data[$key] = explode(",", $input);
        }

        $tracker["updated_at"] = new \MongoDB\BSON\UTCDateTime(new \DateTime('now'));
        $tracker = DB::connection('tracker')->collection('tracker')->where("_id", "=", $tracker_id)->update($data);
        return $tracker;
    }

    public function totalEmails($limit = 1000)
    {
        $emails = TrackerModel::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => ['$size' => ['$ifNull' => ['$emails', []]]]]
                    ]
                ]
            ]);
        })->take($limit);

        return count($emails) > 0 ? $emails->first()->total : 0;
    }

    public function totalProducts($limit = 1000)
    {
        $products = TrackerModel::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => ['$size' => ['$ifNull' => ['$products', []]]]]
                    ]
                ]
            ]);
        })->take($limit);

        return count($products) > 0 ? $products->first()->total : 0;
    }

    public function totalOneSignals($limit = 1000)
    {
        $one_signals = TrackerModel::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => ['$size' => ['$ifNull' => ['$one_signal', []]]]]
                    ]
                ],
            ]);
        })->take($limit);

        return count($one_signals) > 0 ? $one_signals->first()->total : 0;
    }

    public function totalGeolocations($limit = 1000)
    {
        $geo = TrackerModel::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => ['$size' => ['$ifNull' => ['$geolocation', []]]]]
                    ]
                ]
            ]);
        })->take($limit);

        return count($geo) > 0 ? $geo->first()->total : 0;
    }

    public function search(array $queries)
    {
        $data = DB::connection('tracker')->collection('tracker');
        foreach ($queries AS $i) {
            if ($i["operator"] == "all") {
                $data->where($i["column"], 'regexp', '/.*' . $i["term"] . '/i');
            } else {
                $data->where($i["column"], $i["operator"], $i["term"]);
            }
        }
        return $data->distinct()->orderBy("updated_at","DESC");
    }

    public function deleteInvalidRegisters()
    {
        set_time_limit(0);

        TrackerModel::
        whereNull("emails")
            ->whereNull("one_signal")
            ->delete();

        return "deleteds";
    }

    public function crossingData(array $trackerWheres, $table, array $tableWheres, $columnUpdate){
        $data = TrackerModel::query();
        $update = DB::table($table);

        foreach($wheres AS $w){
            $data->where($w[0],$w[1],$w[2]);
        }

        foreach($data->get() AS $d){
            foreach($tableWheres AS $wh){
                $value = isset($data[$wh[2]]) && is_array($data[$wh[2]]) ? key(end($data[$wh[2]])) : $data[$wh[2]];
                $update->where($wh[0],$wh[1],$value)
                    ->update([
                        $columnUpdate => $data[$wh[2]]
                    ]);
            }
        }
    }
}