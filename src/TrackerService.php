<?php
namespace KelvynCarbone\Tracker;
use Illuminate\Support\Facades\DB;
use KelvynCarbone\Tracker\Models\Tracker;

class TrackerService {
    public function create(array $inputs){
        $tracker = new Tracker();
        foreach ($inputs AS $key => $input) {
            if($key=="geolocation" && is_array($input))
                $data["loc"] = ["x" => $input[0], "y" => $input[1]];
            else
                $tracker[$key] = explode(",", $input);
        }
        $tracker->save();
        return $tracker;
    }

    public function update(array $inputs){
        $tracker_id = $inputs["tracker_id"];
        unset($inputs["tracker_id"]);
        foreach ($inputs AS $key => $input){
            if($key=="geolocation" && is_array($input))
                $data["loc"] = ["x" => $input[0], "y" => $input[1]];
            else
                $data[$key] = explode(",", $input);
        }

        $tracker = DB::connection('tracker')->collection('tracker')->where("_id","=",$tracker_id)->update($data);
        return $tracker;
    }

    public function totalEmails(){
        $emails = Tracker::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => ['$size' => ['$ifNull' => [ '$emails', [] ]]]]
                    ]
                ]
            ]);
        });

        return count($emails)>0 ? $emails->first()->total : 0;
    }

    public function totalProducts(){
        $products = Tracker::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => ['$size' => ['$ifNull' => [ '$products', [] ]]]]
                    ]
                ]
            ]);
        });

        return count($products)>0 ? $products->first()->total : 0;
    }

    public function totalOneSignals(){
        $one_signals = Tracker::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => ['$size' => ['$ifNull' => [ '$one_signal', [] ]]]]
                    ]
                ],
            ]);
        });

        return count($one_signals)>0 ? $one_signals->first()->total : 0;
    }

    public function totalGeolocations(){
        $geo = Tracker::raw(function ($collection) {
            return $collection->aggregate([
                [
                    '$group' => [
                        '_id' => null,
                        'total' => ['$sum' => '$loc']
                    ]
                ]
            ]);
        });

        return count($geo) > 0 ? $geo->first()->total : 0;
    }

    public function grid(){
        $data = DB::connection('tracker')->collection('tracker')->whereNotNull("one_signal");

        return count($data) > 0 ? $data->get() : 0;
    }
}