// Tracker Plugin - By: Kelvyn Carbone
(function () {
    // Define our constructor
    this.tracker = function (options) {
        // var host = "https://tracker.acasadoconcurseiro.com.br/api/v1";
        var host = "http://admin.local.com/api/v1";
        var product = window.location.hostname;
        if(localStorage.getItem("t_data")) {
            var  t_data = JSON.parse(localStorage.getItem("t_data"));
        }else{
            var data_atual = new Date();
            data_atual = data_atual.getFullYear()+'-'+(data_atual.getMonth()+1)+data_atual.getMonth()+1+data_atual.getDate();
            localStorage.setItem("t_data",'{"products":{"'+product+'":{"urls": [{"url": "'+product+window.location.pathname+'","visits": 1,"created_at": "'+data_atual+'"}]}}}');
            var  t_data = JSON.parse(localStorage.getItem("t_data"));
        }

        this.validateEmail = function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        };
        //Function to get querystrings
        this.getParameterByName = function (name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        };

        //Function to make a xh request
        this.request = function (method, url) {
            var xhr = new XMLHttpRequest();
            if (!url.match("auth") && localStorage.getItem("t_token"))
                url = url + "?token=" + localStorage.getItem("t_token");

            if ("withCredentials" in xhr) {
                // Most browsers.
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest != "undefined") {
                // IE8 & IE9
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported.
                xhr = null;
            }
            return xhr;
        };

        this.setData = function(property,value){
            if(t_data.hasOwnProperty(t_data["products"][product][property]) && t_data["products"][product][property] instanceof Array) {
                t_data["products"][product][property].push(value);
            }
            else {
                t_data["products"][product][property] = value;
            }

            localStorage.setItem("t_data",JSON.stringify(t_data));
        };

        this.setToken = function (callbackSuccess) {
            var request = this.request("POST", host + "/auth");
            request.onreadystatechange = function (oEvent) {
                if (request.readyState === 4) {
                    if (request.status === 200) {
                        var rs = JSON.parse(request.response);
                        if (rs.token)
                            localStorage.setItem("t_token", rs.token);
                        if (callbackSuccess)
                            callbackSuccess(rs);
                    } else if (request.status === 205) {
                        localStorage.removeItem("t_data");
                    }
                    else {
                        console.log("TRACKER - We can't get api token", request.statusText);
                    }
                }
            };

            var inputs = new FormData();
            inputs.append("email", "tracker@uoledtech.com");
            inputs.append("password", "uol@432$!");
            request.send(inputs);
        };

        this.dataExist = function (value) {
            var _data = localStorage.getItem("t_data") ? localStorage.getItem("t_data") : null;
            if (!_data)
                return false;

            return _data.indexOf(value) > -1 ? true : false;
        };

        this.emailEvent = function () {
            var tracker_email = localStorage.getItem("t_data") && JSON.parse(localStorage.getItem("t_data"))["emails"] ? JSON.parse(localStorage.getItem("t_data"))["emails"][0] : null;
            var inputs = document.getElementsByName("email");
            inputs.forEach(function (input, index) {
                input.addEventListener("focusout", function (e) {
                    var email = e.target.value;
                    if (validateEmail(email) && email !== tracker_email)
                        setData("emails", email);
                });
            });
        };

        this.namesEvent = function () {
            var inputs = document.getElementsByName("name") || document.getElementsByName("nome");
            var tracker_name = localStorage.getItem("t_data") && JSON.parse(localStorage.getItem("t_data"))["names"] ? JSON.parse(localStorage.getItem("t_data"))["names"][0] : null;
            inputs.forEach(function (input, index) {
                input.addEventListener("focusout", function (e) {
                    var name = e.target.value;
                    if (name && name !== tracker_name)
                        setData("names", name);
                });
            });
        };

        this.getOneSignal = function () {
            if (typeof(OneSignal) === "undefined")
                return false;
            if (typeof(OneSignal.getUserId) === "undefined")
                return false;

            OneSignal.on('subscriptionChange', function (isSubscribed) {
                OneSignal.getUserId(function (user_id) {
                    if (user_id && user_id !== onesignal)
                        setData("onesignal", user_id);
                });
            });

            OneSignal.getUserId(function (user_id) {
                if (user_id)
                    setData("onesignal", user_id);
            });
        };

        this.geolocationCapture = function () {
            if (!navigator.geolocation) {
                console.log('TRACKER - Geolocation is not supported for this Browser/OS.');
                return false;
            }
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = localStorage.getItem("t_data") && JSON.parse(localStorage.getItem("t_data"))["geolocation"] ? JSON.parse(localStorage.getItem("t_data"))["geolocation"] : null;
                if (geolocation && geolocation.length > 0) {
                    this.setCookie("latitude", position.coords.latitude, 700);
                    this.setCookie("longitude", position.coords.longitude, 700);
                    geolocation.forEach(function (item, index) {
                        JSON.stringify(item) !== JSON.stringify([position.coords.latitude, position.coords.longitude]) ? delete geolocation[index] : setData("geolocation", geolocation);
                    });
                } else {
                    if (geolocation || (geolocation && geolocation[0].includes(position.coords.latitude) && geolocation[0].includes(position.coords.longitude)))
                        return false;

                    setData("geolocation", [position.coords.latitude, position.coords.longitude]);
                    this.setCookie("latitude", position.coords.latitude, 700);
                    this.setCookie("longitude", position.coords.longitude, 700);
                }
            });
        };

        this.facebookEvents = function () {
            var tries = 1;
            var tryFB = window.setInterval(function () {
                if (++tries === 2) {
                    window.clearInterval(tryFB);
                }
                try {
                    if (typeof FB !== "undefined") {
                        FB.init({
                            appId: document.head.querySelector("meta[property~='fb:app_id'][content]").content,
                            status: true, // check login status
                            cookie: true, // enable cookies to allow the server to access the session
                            xfbml: true,  // parse XFBML
                            version: 'v2.7'
                        });

                        FB.getLoginStatus(function (response) {
                            if (response.status == 'connected') {
                                getCurrentUserInfo(response)
                            }
                        });

                        function getCurrentUserInfo() {
                            var email = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["emails"] ? JSON.parse(localStorage.getItem("tracker_data"))["emails"][0] : null;
                            var name = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["names"] ? JSON.parse(localStorage.getItem("tracker_data"))["names"][0] : null;
                            FB.api('/me', {fields: 'name, email'}, function (userInfo) {
                                if (userInfo.name && userInfo.name !== "" && userInfo.name !==name)
                                    setData("names", userInfo.name);
                                if (userInfo.email && userInfo.email !== "" && userInfo.email !==email)
                                    setData("emails", userInfo.email);
                            });
                        }
                    }
                } catch (e) {
                    window.clearInterval(tryFB);
                    console.log("TRACKER - We can't get facebook scopes: "+e);
                }
            }, 3000);
        };

        this.setUrl = function(){

        };

        this.initCaptureEvents = function () {
            setToken(function () {
                // if(!dataExist(product))
                geolocationCapture();
                getOneSignal();
                emailEvent();
                namesEvent();
                facebookEvents();
                setUrl();
            });
        };

        this.initCaptureEvents();
    };
}());
tracker();


// localStorage.setItem("t_data", JSON.stringify({
//     "tracker_id": 12132132123,
//     "products": {
//         "acasadoconcurseiro.com.br": {
//             "emails": [
//                 "kim_kelvyn@hotmail.com",
//                 "kelvyn.carbone@gmail.com"
//             ],
//             "names": [
//                 "Kelvyn Carbone",
//                 "Kelvyn"
//             ],
//             "geolocation": {
//                 "latitude": 2423123123,
//                 "longitude": 456456456
//             },
//             "onesignal": "112323132-123123-12313213",
//             "urls": [{
//                 "url": "acasadoconcurseiro.com.br/concurso/bnb",
//                 "visits": 4,
//                 "created_at": "2018-08-24 10:40",
//                 "updated_at": "2018-08-24 13:40"
//             }],
//             "conversions": {
//
//                 "email": "kim_kelvyn@hotmail.com",
//                 "url": "acasadoconcurseiro.com.br/concurso/bnb",
//                 "product": "MPU",
//                 "price": 551,
//                 "conversion_date": "2018-08-24 15:40"
//             }
//         },
//         "estudaquepassa.com.br": {
//             "emails": [
//                 "kim_kelvyn@hotmail.com",
//                 "kelvyn.carbone@gmail.com"
//             ],
//             "names": [
//                 "Kelvyn Carbone",
//                 "Kelvyn"
//             ],
//             "geolocation": {
//                 "latitude": 2423123123,
//                 "longitude": 456456456
//             },
//             "onesignal": "112323132-123123-12313213",
//             "urls": [{
//                 "url": "estudaquepassa.com.br/concurso/bnb",
//                 "visits": 4,
//                 "created_at": "2018-08-24 10:40",
//                 "updated_at": "2018-08-24 13:40"
//             }],
//             "conversions": {
//
//                 "email": "kim_kelvyn@hotmail.com",
//                 "url": "estudaquepassa.com.br/concurso/bnb",
//                 "product": "MPU",
//                 "price": 551,
//                 "conversion_date": "2018-08-24 15:40"
//             }
//         }
//     }
// }));