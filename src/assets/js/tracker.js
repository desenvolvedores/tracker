// Tracker Plugin - By: Kelvyn Carbone
(function () {
    // Define our constructor
    this.tracker = function (options) {
        var host = "https://tracker.acasadoconcurseiro.com.br/api/v1";

        this.validateEmail = function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        };
        //Function to get querystrings
        this.getParameterByName = function (name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        };

        //Function to make a xh request
        this.request = function (method, url) {
            var xhr = new XMLHttpRequest();
            if (!url.match("auth") && localStorage.getItem("tracker_token"))
                url = url + "?token=" + localStorage.getItem("tracker_token");

            if ("withCredentials" in xhr) {
                // Most browsers.
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest != "undefined") {
                // IE8 & IE9
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported.
                xhr = null;
            }
            return xhr;
        };

        this.extendDefaults = function (source, properties) {
            var property;
            for (property in properties) {
                if (properties.hasOwnProperty(property)) {
                    source[property] = properties[property];
                }
            }
            return source;
        };

        this.setData = function (key, value, without_request) {
            var data = JSON.parse(localStorage.getItem("tracker_data"));
            without_request = without_request ? true : false;

            if (!data)
                data = {};

            if (key === "tracker_id")
                data["tracker_id"] = value;
            else if (data[key]) {
                if (key == "geolocation") {
                    var locations_saved = Object.values(data[key]);
                    locations_saved.forEach(function (input, index) {
                        if (JSON.stringify(locations_saved) !== JSON.stringify(value))
                            data[key].push(value)
                    });
                }
                else {
                    data[key] && Object.values(data[key]).indexOf(value) == -1 ? data[key].push(value) : null;
                }
            }
            else
                data[key] = [value];

            localStorage.setItem("tracker_data", JSON.stringify(data));

            if (without_request === false) {
                var request = this.request("POST", host + "/tracker");
                request.onreadystatechange = function (oEvent) {
                    if (request.readyState === 4) {
                        if (request.status === 200) {
                            var rs = JSON.parse(request.response);
                            if (typeof(rs.tracker_id) !== "undefined")
                                setData("tracker_id", rs.tracker_id, true);
                        } else if (request.status === 205) {
                            localStorage.removeItem("tracker_data");
                        }
                        else {
                            console.log("TRACKER - We can't send data to tracker route, error: ", request.statusText);
                        }
                    }
                };
                var inputs = new FormData();
                for (var key in data) {
                    inputs.append(key, data[key]);
                }
                request.send(inputs);
            }
        };

        this.emailEvent = function () {
            var tracker_email = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["emails"] ? JSON.parse(localStorage.getItem("tracker_data"))["emails"][0] : null;
            var inputs = document.getElementsByName("email");
            inputs.forEach(function (input, index) {
                input.addEventListener("focusout", function (e) {
                    var email = e.target.value;
                    if (validateEmail(email) && email!==tracker_email)
                        setData("emails", email);
                });
            });
        };

        this.namesEvent = function () {
            var inputs = document.getElementsByName("name") || document.getElementsByName("nome");
            var tracker_name = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["names"] ? JSON.parse(localStorage.getItem("tracker_data"))["names"][0] : null;
            inputs.forEach(function (input, index) {
                input.addEventListener("focusout", function (e) {
                    var name = e.target.value;
                    if (name && name !== tracker_name)
                        setData("names", name);
                });
            });
        };

        this.setCookie = function (cname, cvalue, exdays) {
            var d = new Date(); //Create an date object
            d.setTime(d.getTime() + (exdays * 1000 * 60 * 60 * 24)); //Set the time to exdays from the current date in milliseconds. 1000 milliseonds = 1 second
            var expires = "expires=" + d.toGMTString(); //Compose the expirartion date
            window.document.cookie = cname + "=" + cvalue + "; " + expires;//Set the cookie with value and the expiration date
        };

        this.OneSignalEvents = function () {
            var onesignal = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["one_signal"] ? JSON.parse(localStorage.getItem("tracker_data"))["one_signal"][0] : null;

            if (typeof(OneSignal) === "undefined")
                return false;
            if (typeof(OneSignal.getUserId) === "undefined")
                return false;

            OneSignal.on('subscriptionChange', function (isSubscribed) {
                OneSignal.getUserId(function (user_id) {
                    if (user_id && user_id !== onesignal)
                        setData("one_signal", user_id);
                });
            });

            OneSignal.getUserId(function (user_id) {
                if (user_id && user_id !== onesignal)
                    setData("one_signal", user_id);
            });
        };

        this.geolocationCapture = function () {
            if (!navigator.geolocation) {
                console.log('TRACKER - Geolocation is not supported for this Browser/OS.');
                return false;
            }
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["geolocation"] ? JSON.parse(localStorage.getItem("tracker_data"))["geolocation"] : null;
                if (geolocation && geolocation.length > 0) {
                    this.setCookie("latitude", position.coords.latitude, 700);
                    this.setCookie("longitude", position.coords.longitude, 700);
                    geolocation.forEach(function (item, index) {
                        JSON.stringify(item) !== JSON.stringify([position.coords.latitude, position.coords.longitude]) ? delete geolocation[index] : setData("geolocation", geolocation);
                    });
                } else {
                    if(geolocation || (geolocation && geolocation[0].includes(position.coords.latitude) && geolocation[0].includes(position.coords.longitude)))
                        return false;

                    setData("geolocation", [position.coords.latitude, position.coords.longitude]);
                    this.setCookie("latitude", position.coords.latitude, 700);
                    this.setCookie("longitude", position.coords.longitude, 700);
                }
            });
        };

        this.setToken = function (callbackSuccess) {
            var request = this.request("POST", host + "/auth");
            request.onreadystatechange = function (oEvent) {
                if (request.readyState === 4) {
                    if (request.status === 200) {
                        var rs = JSON.parse(request.response);
                        if (rs.token)
                            localStorage.setItem("tracker_token", rs.token);
                        if (callbackSuccess)
                            callbackSuccess(rs);
                    } else if (request.status === 205) {
                        localStorage.removeItem("tracker_data");
                    }
                    else {
                        console.log("TRACKER - We can't get api token", request.statusText);
                    }
                }
            };

            var inputs = new FormData();
            inputs.append("email", "tracker@uoledtech.com");
            inputs.append("password", "uol@432$!");
            request.send(inputs);
        };

        this.facebookEvents = function () {
            var tries = 1;
            var tryFB = window.setInterval(function () {
                try {
                    if (typeof FB !== "undefined") {
                        FB.init({
                            appId: $("meta[property~='fb:app_id']").attr("content"),
                            status: true, // check login status
                            cookie: true, // enable cookies to allow the server to access the session
                            xfbml: true,  // parse XFBML
                            version: 'v2.8'
                        });

                        FB.getLoginStatus(function (response) {
                            if (response.status == 'connected') {
                                getCurrentUserInfo(response)
                            }
                        });

                        function getCurrentUserInfo() {
                            var email = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["emails"] ? JSON.parse(localStorage.getItem("tracker_data"))["emails"][0] : null;
                            var name = localStorage.getItem("tracker_data") && JSON.parse(localStorage.getItem("tracker_data"))["names"] ? JSON.parse(localStorage.getItem("tracker_data"))["names"][0] : null;
                            FB.api('/me', {fields: 'name, email'}, function (userInfo) {
                                if (userInfo.name && userInfo.name !== "" && userInfo.name !==name)
                                    setData("names", userInfo.name);
                                if (userInfo.email && userInfo.email !== "" && userInfo.email !==email)
                                    setData("emails", userInfo.email);

                                if (++tries === 2) {
                                    window.clearInterval(tryFB);
                                }
                            });
                        }
                    }
                } catch (e) {
                    console.log("TRACKER - We can't get facebook scopes: "+e);
                }
            }, 3000);
        };

        this.initCaptureEvents = function () {
            setToken(function () {
                facebookEvents();
                OneSignalEvents();
                emailEvent();
                var url = location.pathname && location.pathname !== "/" ? location.host + location.pathname : location.host;
                if(localStorage.getItem("tracker_data")){
                    var data = JSON.parse(localStorage.getItem("tracker_data"));
                    if(data && data["products"] && !data["products"].includes(url))
                        setData("products", url);
                    else if(location.pathname !== "/" && !data["products"])
                        setData("products", url);
                }


                geolocationCapture();
                namesEvent();
            });
        };

        this.initCaptureEvents();
    };
}());
tracker();


