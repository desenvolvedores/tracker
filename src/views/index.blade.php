<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tracker</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <style>
        .label{
            margin:5px;
        }
        .pointer{
            cursor: pointer;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Tracker</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        Preparatórios <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/tracker/relatorios/casa/por-interesse">Relatório por interesse</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        Educação Formal <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/tracker/relatorios/pos/por-interesse">Relatório por interesse</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container" style="margin-top:10px;">
    <div class="row">
        <main role="main" class="well">
            <div class="">
                <div class="alert alert-dark" role="alert" id="chart-div">
                </div>
                {!! $lava->render('BarChart', 'IMDB', 'chart-div')!!}
            </div>
        </main>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label>Campo</label>
            <select name="column" class="form-control">
                <option value="products">URL's</option>
                <option value="onesignal">One Signal</option>
                <option value="names">Names</option>
                <option value="emails">E-mails</option>
            </select>
        </div>
        <div class="col-md-3">
            <label>Condição</label>
            <select name="operator" class="form-control">
                <option value="all">Contém</option>
                <option value="=">Igual</option>
                <option value="<>">Diferente</option>
                <option value=">=">Maior ou igual a</option>
                <option value="<=">Menor ou igual a</option>
            </select>
        </div>
        <div class="col-md-3">
            <label>Termo / Palavra-chave</label>
            <input type="text" placeholder="Termo/Palavra-chave" name="term" class="form-control">
        </div>
        <div class="col-md-2">
            <br>
            <button type="button" class="btn btn-primary btn-block" id="add-tags">Filtrar</button>
        </div>

    </div>
    <div class="row">
        <br><br>
        <form name="search" action="/tracker" method="GET">
            <div class="col-md-12 @if(isset($inputs) && isset($inputs["fields"]) && is_array($inputs["fields"])) well @endif"
                 id="tags">
                @if(isset($inputs) && isset($inputs["fields"]) && is_array($inputs["fields"]))
                    <?php $count = 0; ?>
                    @foreach($inputs["fields"] AS $key => $f)
                        <label class="label label-danger">
                            <input type="hidden" name="fields[{{$count}}][column]" value="{{$f["column"]}}">
                            <input type="hidden" name="fields[{{$count}}][operator]" value="{{$f["operator"]}}">
                            <input type="hidden" name="fields[{{$count}}][term]" value="{{$f["term"]}}">
                            {{$f["column"]." - ".$f["term"]}}
                            <span class="glyphicon glyphicon-remove pointer remove-tag"></span>
                        </label>
                    @endforeach
                @endif
            </div>
        </form>
    </div>
    <div class="col-md-12 text-center">
        @if($data)
        {{ $data->appends(Illuminate\Support\Facades\Input::all())->links() }}
        @endif
    </div>
    <hr>
</div>

<div class="container">
    <div class="table-responsive">
        <table class="table table-hover table-bordered ">
            <thead style="background: #e9e9e9; vertical-align: middle;">
            <th>Ações</th>
            <th>Nomes/Emails</th>
            <th>One Signals</th>
            <th>URL's</th>
            <th>Data Atualização</th>
            </thead>
            <tbody>
            @forelse($data AS $d)
                <?php $date = (string)$d["updated_at"]; ?>
                <tr>
                    <td>
                        @if(isset($d["one_signal"]))
                            <a target="_blank" class="btn btn-danger"
                               href="/onesignal?players[{{implode($d["one_signal"],",")}}]">Criar Notificação</a>
                        @endif
                    </td>
                    <td>
                        {!!  isset($d["names"]) && is_array($d["names"]) ? implode($d["names"],"<br>") : @$d["names"] !!}
                        <hr>
                        {!! isset($d["emails"]) && is_array($d["emails"]) ? implode($d["emails"],",<br>") : @$d["emails"]!!}
                    </td>
                    <td>{!! isset($d["one_signal"]) && is_array($d["one_signal"]) ? implode($d["one_signal"],"<br>") : @$d["one_signal"] !!}</td>
                    <td>{!!  isset($d["products"]) && is_array($d["products"]) ? implode($d["products"],"<br>") : @$d["products"] !!}</td>
                    <td>{{ isset($d["updated_at"]) ? Carbon\Carbon::createFromTimestampMs($date)->format("d/m/Y H:i") : ""}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">Nenhum registro encontrado...</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
<div class="container" style="margin-top:10px;">
    <div class="row">
        <div class="col-md-12 text-center">
            @if($data)
            {{ $data->appends(Illuminate\Support\Facades\Input::all())->links() }}
            @endif
        </div>
    </div>
</div>
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<script>
    $(document).ready(function () {
        window.count = '{{isset($inputs['fields']) ? count($inputs['fields']) : 0}}';
        $("#add-tags").on("click", function () {
            var column = $("select[name=column]").val();
            var operator = $("select[name=operator]").val();
            var term = $("input[name=term]").val();

            var label = '<label class="label label-danger">';
            label += '<input type="hidden" name="fields['+window.count+'][column]" value="' + column + '">';
            label += '<input type="hidden" name="fields['+window.count+'][operator]" value="' + operator + '">';
            label += '<input type="hidden" name="fields['+window.count+'][term]" value="' + term + '">';
            label += column + " - " + term;
            label += '<span class="glyphicon glyphicon-remove"></span>';
            label += '</label>';

            $("#tags").append(label);
            $("form[name=search]").submit();
        });

        $(".remove-tag").on("click", function () {
           $(this).parent().remove();
            $("form[name=search]").submit();
        });
    });
</script>
</body>
</html>
